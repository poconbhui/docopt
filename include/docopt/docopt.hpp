#ifndef DOCOPT_DOCOPT_HPP
#define DOCOPT_DOCOPT_HPP

#include <algorithm>
#include <cassert>
#include <cstdint>
#include <iostream>
#include <iterator>
#include <memory>
#include <regex>
#include <string>
#include <vector>

namespace docopt {

namespace detail {
    constexpr const char* whitespace = " \n";

    // Python's str.partition()
    struct partition {
        std::string before;
        std::string separator;
        std::string after;

        partition(const std::string& to_partition, const std::string& separator) {
            auto partition_pos = to_partition.find(separator);

            before = to_partition.substr(0, partition_pos);
            if(partition_pos != std::string::npos) {
                this->separator = to_partition.substr(
                    partition_pos, separator.size()
                );
                after = to_partition.substr(partition_pos + separator.size());

                assert(this->separator == separator);
            }
        }
    };

    // Python's str.strip()
    std::string strip(const std::string& to_strip) {
        auto initial_spaces = to_strip.find_first_not_of(whitespace);
        auto trailing_spaces = to_strip.find_last_not_of(whitespace);

        auto stripped = to_strip.substr(
            initial_spaces, trailing_spaces - initial_spaces + 1
        );

        assert(
            stripped.length() == 0
            || (stripped.front() != ' ' && stripped.back() != ' ')
        );

        return stripped;
    }

    // Python's str.startswith
    bool startswith(const std::string& str, const std::string prefix) {
        return str.compare(0, prefix.length(), prefix) == 0;
    }

    // Python's str.split()
    std::vector<std::string> split(const std::string& to_split) {
        std::vector<std::string> value;
        size_t start_pos = to_split.find_first_not_of(whitespace);
        while(start_pos != std::string::npos) {
            auto pos = to_split.find_first_of(whitespace, start_pos);
            if(pos == std::string::npos) {
                value.emplace_back(to_split.substr(start_pos));
                break;
            }
            value.emplace_back(to_split.substr(start_pos, pos - start_pos));
            start_pos = to_split.find_first_not_of(whitespace, pos);
        }

        return value;
    }

    // Python's re.split with capture groups
    std::vector<std::string> split(
        const std::regex& re, const std::string& s
    ) {
        std::vector<std::string> result;

        std::sregex_token_iterator s_match_it(
            s.cbegin(), s.cend(), re, 1
        );
        std::sregex_token_iterator s_miss_it(
            s.cbegin(), s.cend(), re, -1
        );
        std::sregex_token_iterator s_end;

        if(s_miss_it != s_end) {
            result.emplace_back(*s_miss_it);
            ++s_miss_it;
        }

        while(s_match_it != s_end && s_miss_it != s_end) {
            result.emplace_back(*s_match_it);
            result.emplace_back(*s_miss_it);

            ++s_match_it;
            ++s_miss_it;
        }

        return result;
    }

    // Python's re.findall
    std::vector<std::string> findall(
        const std::regex& re, const std::string& s
    ) {
        std::vector<std::string> result;

        std::smatch matches;

        auto s_it = s.cbegin();
        const auto s_end = s.cend();
        while(std::regex_search(s_it, s_end, matches, re)) {
            if(matches.size() == 1) result.emplace_back(matches[0]);
            if(matches.size() > 1) result.emplace_back(matches[1]);
            s_it = matches.suffix().first;
        }

        return result;
    }

    // Python's str.join (with transformation of the iterable)
    template<typename Range, typename Transform>
    std::string join(
        std::string separator, Range&& range, Transform&& transform
    ) {
        std::string result;

        auto begin = std::begin(std::forward<Range>(range));
        auto end = std::end(std::forward<Range>(range));
        if(begin == end) return result;

        result += transform(*begin);
        for(auto it = std::next(begin); it != end; ++it) {
            result += separator + transform(*it);
        }

        return result;
    }
}


/// Exit in case user invoked program with incorrect arguments.
class DocoptExit {
    std::string usage_;
public:

    DocoptExit(std::string usage = ""):
        usage_{usage}
    {}

    void error() {
        std::cerr << '\n' << usage_;
        std::exit(1);
    }

    template<typename Message, typename... Messages>
    void error(Message&& message, Messages&&... messages) {
        std::cerr << message;
        error(std::forward<Messages>(messages)...);
    }
};


class value {
    std::string value_;

    enum class which: uint8_t {
        string_value, bool_value, none_value
    };
    which which_value;
public:

    value(std::string value):
        value_{value},
        which_value(which::string_value)
    {}

    value(const char* value):
        docopt::value(std::string(value))
    {}

    value(bool value):
        value_{value ? "True" : "False"},
        which_value(which::bool_value)
    {}

    value():
        value_{"None"},
        which_value(which::none_value)
    {}

    friend bool operator==(const value& a, const value& b) {
        return a.which_value == b.which_value && a.value_ == b.value_;
    }

    friend bool operator!=(const value& a, const value& b) {
        return a.which_value != b.which_value || a.value_ != b.value_;
    }
        
    const std::string& repr() const {
        return value_;
    }

    friend std::ostream& operator<<(std::ostream& os, const value& v) {
        os << v.repr();
        return os;
    }
};


class Pattern {
protected:
    class Interface:
        public std::enable_shared_from_this<Pattern::Interface>
    {
    public:
        virtual ~Interface() = default;

        virtual std::string type() const = 0;

        virtual std::string name() const = 0;

        virtual std::string repr() const = 0;

        virtual std::vector<Pattern>
        flat(const std::vector<std::string>& types = {}) const = 0;
    };

    std::shared_ptr<const Pattern::Interface> pattern_interface_;

protected:

    template<typename T, typename... Args>
    static std::shared_ptr<const Pattern::Interface> make_pattern_interface(
        Args&&... args
    ) {
        return std::shared_ptr<const Pattern::Interface>(
            std::make_shared<T>(std::forward<Args>(args)...)
        );
    }


public:
    // Construct from a shared_ptr to Pattern::Interface
    Pattern(
        std::shared_ptr<const Pattern::Interface> pattern_interface
    ):
        pattern_interface_(std::move(pattern_interface))
    {}

    std::string type() const { return pattern_interface_->type(); }

    std::string name() const { return pattern_interface_->name(); }

    std::string repr() const { return pattern_interface_->repr(); }

    std::vector<Pattern> flat(
        const std::vector<std::string>& types = {}
    ) const {
        return pattern_interface_->flat(types);
    }

    bool operator==(const Pattern& other) const {
        return repr() == other.repr();
    }

    bool operator!=(const Pattern& other) const {
        return repr() != other.repr();
    }

    friend std::ostream& operator<<(std::ostream& os, const Pattern& pattern) {
        os << pattern.repr();
        return os;
    }

//    def __hash__(self):
//        return hash(repr(self))
//
//    def fix(self):
//        self.fix_identities()
//        self.fix_repeating_arguments()
//        return self
//
//    def fix_identities(self, uniq=None):
//        """Make pattern-tree tips point to same object if they are equal."""
//        if not hasattr(self, 'children'):
//            return self
//        uniq = list(set(self.flat())) if uniq is None else uniq
//        for i, child in enumerate(self.children):
//            if not hasattr(child, 'children'):
//                assert child in uniq
//                self.children[i] = uniq[uniq.index(child)]
//            else:
//                child.fix_identities(uniq)
//
//    def fix_repeating_arguments(self):
//        """Fix elements that should accumulate/increment values."""
//        either = [list(child.children) for child in transform(self).children]
//        for case in either:
//            for e in [child for child in case if case.count(child) > 1]:
//                if type(e) is Argument or type(e) is Option and e.argcount:
//                    if e.value is None:
//                        e.value = []
//                    elif type(e.value) is not list:
//                        e.value = e.value.split()
//                if type(e) is Command or type(e) is Option and e.argcount == 0:
//                    e.value = 0
//        return self
};


/// Leaf/terminal node of a pattern tree.
class LeafPattern: public Pattern {
protected:
    class Interface: public Pattern::Interface {
        std::string name_;
        docopt::value value_;
    public:

        Interface(std::string name, docopt::value value = ""):
            Pattern::Interface(),
            name_(std::move(name)), value_(std::move(value))
        {}

        std::string name() const final { return name_; }
        virtual docopt::value value() const { return value_; }

        std::string repr() const override {
            return type() + "("
                + name_ + ", " + value_.repr() + ")";
        }


        std::vector<Pattern> flat(
            const std::vector<std::string>& types = {}
        ) const final {
            if(types.empty()) {
                return {Pattern(shared_from_this())};
            }
            for(const auto& input_type: types) {
                if(type() == input_type) {
                    return {Pattern(shared_from_this())};
                }
            }
            return {};
        }

//        def match(self, left, collected=None):
//            collected = [] if collected is None else collected
//            pos, match = self.single_match(left)
//            if match is None:
//                return False, left, collected
//            left_ = left[:pos] + left[pos + 1:]
//            same_name = [a for a in collected if a.name == self.name]
//            if type(self.value) in (int, list):
//                if type(self.value) is int:
//                    increment = 1
//                else:
//                    increment = ([match.value] if type(match.value) is str
//                                 else match.value)
//                if not same_name:
//                    match.value = increment
//                    return True, left_, collected + [match]
//                same_name[0].value += increment
//                return True, left_, collected
//            return True, left_, collected + [match]

    };

    using Pattern::Pattern;

public:
    docopt::value value() const {
        return static_cast<const LeafPattern::Interface*>(
            pattern_interface_.get()
        )->value();
    }
};


/// Branch/inner node of a pattern tree.
class BranchPattern: public Pattern {
protected:
    class Interface: public Pattern::Interface {
        std::vector<Pattern> children_;
    public:
        template<typename... Children>
        Interface(
            Children&&... children
        ):
            children_{std::forward<Children>(children)...}
        {}

        std::string type() const override {
            return "BranchPattern";
        }

        std::string name() const final {
            assert(false && "Unexpected call to BranchPattern::name()");
            return "N/A";
        }

        std::string repr() const override {
            std::string r = type() + "(";
            for(const auto& child: children_) {
                r += child.repr();
            }
            r += ")";

            return r;
        }

        std::vector<Pattern> flat(
            const std::vector<std::string>& types = {}
        ) const override {
            for(const auto& input_type: types) {
                if(type() == input_type) {
                    return {Pattern(shared_from_this())};
                }
            }

            std::vector<Pattern> result;
            for(const auto& child: children_) {
                const auto flattened_child = child.flat(types);
                result.insert(
                    result.end(), flattened_child.begin(), flattened_child.end()
                );
            }

            return result;
        }
    };

    using Pattern::Pattern;

public:
    template<typename... Children>
    BranchPattern(
        Children&&... children
    ):
        BranchPattern(
            make_pattern_interface<const BranchPattern::Interface>(
                std::forward<Children>(children)...
            )
        )
    {}

};


class Argument: public LeafPattern {
protected:
    class Interface: public LeafPattern::Interface {
    public:
        using LeafPattern::Interface::Interface;

        std::string type() const override { return "Argument"; }
    };

    using LeafPattern::LeafPattern;

public:
    template<typename... Args>
    Argument(Args&&... args):
        LeafPattern(
            make_pattern_interface<Argument::Interface>(
                std::forward<Args>(args)...
            )
        )
    {}

//    def single_match(self, left):
//        for n, pattern in enumerate(left):
//            if type(pattern) is Argument:
//                return n, Argument(self.name, pattern.value)
//        return None, None

    static Pattern parse(const std::string& source) {
        std::string name = detail::findall(std::regex("(<\\S*?>)"), source)[0];
        auto values = detail::findall(
            std::regex("\\[default: (.*)\\]", std::regex_constants::icase),
            source
        );

        return Argument(name, (!values.empty()) ? values[0] : "");
    }
};


class Option: public LeafPattern {
protected:
    class Interface: public LeafPattern::Interface {
        std::string short_name_;
        std::string long_name_;
        size_t argcount_;

    public:
        Interface(
            std::string short_name = "",
            std::string long_name = "",
            size_t argcount = 0,
            docopt::value value = false
        ):
            LeafPattern::Interface(
                (!long_name.empty()) ? long_name : short_name,
                (value == false && argcount > 0) ? docopt::value() : value
            ),
            short_name_(short_name),
            long_name_(long_name),
            argcount_(argcount)
        {
            assert(argcount == 0 || argcount == 1 && "Argcount must be 0 or 1");
        }

        std::string type() const override {
            return "Option";
        }

        std::string repr() const override {
            return std::string("Option") + "("
                + short_name_ + ", "
                + long_name_ + ", "
                + std::to_string(argcount_) + ", "
                + value().repr() + ")";
        }

        virtual const std::string& short_name() const { return short_name_; }
        virtual const std::string& long_name() const { return long_name_; }
        virtual size_t argcount() const { return argcount_; }
    };

    using LeafPattern::LeafPattern;

public:
    template<typename... Args>
    Option(Args&&... args):
        Option(
            make_pattern_interface<Option::Interface>(
                std::forward<Args>(args)...
            )
        )
    {}

    const std::string& short_name() const {
        return static_cast<const Option::Interface*>(
            pattern_interface_.get()
        )->short_name();
    }

    const std::string& long_name() const {
        return static_cast<const Option::Interface*>(
            pattern_interface_.get()
        )->long_name();
    }

    size_t argcount() const {
        return static_cast<const Option::Interface*>(
            pattern_interface_.get()
        )->argcount();
    }

    static Pattern parse(std::string option_description) {
        std::string short_name {};
        std::string long_name {};
        size_t argcount = 0;
        docopt::value value = false;

        auto partitioned = detail::partition(
            detail::strip(option_description), "  "
        );
        auto& options = partitioned.before;
        auto& description = partitioned.after;
        std::transform(
            options.begin(), options.end(), options.begin(),
            [](char c) { return (c == ',' || c == '=') ? ' ' : c; }
        );
        for(const auto& s: detail::split(options)) {
            if(s.find("--") == 0) {
                long_name = s;
            }
            else if(s.find("-") == 0) {
                short_name = s;
            }
            else {
                argcount = 1;
            }
        }
        if(argcount > 0) {
            auto matched = detail::findall(
                std::regex("\\[default: (.*)\\]", std::regex_constants::icase),
                description
            );
            value = (!matched.empty()) ? matched[0] : docopt::value();
        }

        return Option(short_name, long_name, argcount, value);
    }

//     def single_match(self, left):
//         for n, pattern in enumerate(left):
//             if self.name == pattern.name:
//                 return n, pattern
//         return None, None
//
};


class Required: public BranchPattern {
protected:
    class Interface: public BranchPattern::Interface {
    public:
        using BranchPattern::Interface::Interface;

        std::string type() const override {
            return "Required";
        }
    };

    using BranchPattern::BranchPattern;

public:
    template<typename... Args>
    Required(Args&&... args):
        Required(
            make_pattern_interface<Required::Interface>(
                std::forward<Args>(args)...
            )
        )
    {}


//    def match(self, left, collected=None):
//        collected = [] if collected is None else collected
//        l = left
//        c = collected
//        for pattern in self.children:
//            matched, l, c = pattern.match(l, c)
//            if not matched:
//                return False, left, collected
//        return True, l, c
};


class Optional: public BranchPattern {
protected:
    class Interface: public BranchPattern::Interface {
    public:
        using BranchPattern::Interface::Interface;

        std::string type() const override {
            return "Optional";
        }
    };

    using BranchPattern::BranchPattern;

public:
    template<typename... Args>
    Optional(Args&&... args):
        Optional(
            make_pattern_interface<Optional::Interface>(
                std::forward<Args>(args)...
            )
        )
    {}


//    def match(self, left, collected=None):
//        collected = [] if collected is None else collected
//        for pattern in self.children:
//            m, left, collected = pattern.match(left, collected)
//        return True, left, collected
};


/// Marker/placeholder for [options] shortcut.
class OptionsShortcut: public Optional {
protected:
    class Interface: public Optional::Interface {
    public:
        using Optional::Interface::Interface;

        std::string type() const override {
            return "OptionsShortcut";
        }
    };

    using Optional::Optional;

public:
    template<typename... Args>
    OptionsShortcut(Args&&... args):
        OptionsShortcut(
            make_pattern_interface<OptionsShortcut::Interface>(
                std::forward<Args>(args)...
            )
        )
    {}
};

class OneOrMore: public BranchPattern {
protected:
    class Interface: public BranchPattern::Interface {
    public:
        using BranchPattern::Interface::Interface;

        std::string type() const override {
            return "OneOrMore";
        }
    };

    using BranchPattern::BranchPattern;

public:
    template<typename... Args>
    OneOrMore(Args&&... args):
        OneOrMore(
            make_pattern_interface<OneOrMore::Interface>(
                std::forward<Args>(args)...
            )
        )
    {}

//    def match(self, left, collected=None):
//        assert len(self.children) == 1
//        collected = [] if collected is None else collected
//        l = left
//        c = collected
//        l_ = None
//        matched = True
//        times = 0
//        while matched:
//            # could it be that something didn't match but changed l or c?
//            matched, l, c = self.children[0].match(l, c)
//            times += 1 if matched else 0
//            if l_ == l:
//                break
//            l_ = l
//        if times >= 1:
//            return True, l, c
//        return False, left, collected
};


class Tokens {
    std::vector<std::string> tokens_;
    DocoptExit error_;
public:
    using error_type = DocoptExit;

    Tokens(std::vector<std::string> source, DocoptExit error):
        tokens_(std::move(source)),
        error_(std::move(error))
    {}

    Tokens(const std::string source, DocoptExit error):
        Tokens(detail::split(source), error)
    {}

//    @staticmethod
//    def from_pattern(source):
//        source = re.sub(r'([\[\]\(\)\|]|\.\.\.)', r' \1 ', source)
//        source = [s for s in re.split('\s+|(\S*<.*?>)', source) if s]
//        return Tokens(source, error=DocoptLanguageError)
//
    std::string move() {
        std::string result = std::move(tokens_.front());
        tokens_.erase(tokens_.begin());
        return result;
    }

    const std::string& current() const { return tokens_.front(); }

    bool empty() const { return tokens_.empty(); }
    size_t size() const { return tokens_.size(); }
    const std::string& operator[](size_t i) const { return tokens_[i]; }

    template<typename... Messages>
    void error(Messages&&... messages) {
        error_.error(std::forward<Messages>(messages)...);
    }
};


/// long ::= '--' chars [ ( ' ' | '=' ) chars ] ;
std::vector<Pattern> parse_long(
    Tokens& tokens, std::vector<Option>& options
) {
    Option result("", "");

    auto partition = detail::partition(tokens.move(), "=");
    auto& long_name = partition.before;
    auto& eq = partition.separator;
    auto& value_str = partition.after;
    assert(detail::startswith(long_name, "--"));

    docopt::value value =
        (eq == "" && value_str == "")
            ? docopt::value() : docopt::value(value_str);

    std::vector<Option> similar;
    for(const auto& o: options) {
        if(o.long_name() == long_name) {
            similar.push_back(o);
        }
    }

    if(std::is_same<Tokens::error_type, DocoptExit>::value && similar.empty()) {
        for(const auto& o: options) {
            if(
                !o.long_name().empty()
                && detail::startswith(o.long_name(), long_name)
            ) {
                similar.push_back(o);
            }
        }
    }
    if(similar.size() > 1) {
        tokens.error(
            long_name, " is not a unique prefix: ",
            detail::join(
                ", ", similar, [](const Option& o) { return o.long_name(); }
            )
        );
    }
    else if(similar.empty()) {
        size_t argcount = (eq == "=") ? 1 : 0;
        result = Option("", long_name, argcount);
        options.push_back(result);
        if(std::is_same<Tokens::error_type, DocoptExit>::value) {
            result = Option(
                "", long_name, argcount, (argcount != 0) ? value : true
            );
        }
    }
    else {
        result = Option(
            similar[0].short_name(), similar[0].long_name(),
            similar[0].argcount(), similar[0].value()
        );
        if(result.argcount() == 0) {
            if(value != docopt::value()) {
                tokens.error(
                    result.long_name(), " must not have an argument"
                );
            }
        }
        else {
            if(value == docopt::value()) {
                if(tokens.current() == "" || tokens.current() == "--") {
                    tokens.error(result.long_name(), " requires argument");
                }
                value = tokens.move();
            }
        }
        if(std::is_same<Tokens::error_type, DocoptExit>::value) {
            result = Option(
                result.short_name(), result.long_name(),
                result.argcount(), (value != docopt::value()) ? value : true
            );
        }
    }
    return {result};
}


/// shorts ::= '-' ( chars )* [ [ ' ' ] chars ] ;
std::vector<Pattern> parse_shorts(
    Tokens& tokens, std::vector<Option>& options
) {
    auto token = tokens.move();
    assert(detail::startswith(token, "-") && !detail::startswith(token, "--"));
    auto left = token.find_first_not_of('-');

    std::vector<Pattern> parsed;
    while(left != std::string::npos && left != token.size()) {
        Option o("");
        std::string short_name = std::string("-") + token[left];
        left++;
        std::vector<Option> similar;
        for(const auto& option: options) {
            if(option.short_name() == short_name) similar.emplace_back(option);
        }
        if(similar.size() > 1) {
            tokens.error(
                short_name, " is specified ambiguously ",
                similar.size(), " times"
            );
        }
        else if(similar.size() < 1) {
            assert(false && "is push_back necessary??");
            o = Option(short_name, "", 0);
            //options.push_back(o);
            if(std::is_same<Tokens::error_type, DocoptExit>::value) {
                o = Option(short_name, "", 0, true);
            }
        }
        else {
            o = Option(short_name, similar[0].long_name(),
                       similar[0].argcount(), similar[0].value());
            docopt::value value{};
            if(o.argcount() !=0) {
                if(left == std::string::npos || left == tokens.size()) {
                    if(
                        tokens.empty()
                        || (!tokens.empty() && tokens.current() == "--")
                    ) {
                        tokens.error(short_name, " requires argument");
                    }
                    value = docopt::value(tokens.move());
                }
                else {
                    value = docopt::value(token.substr(left));
                    left = tokens.size();
                }
            }
            if(std::is_same<Tokens::error_type, DocoptExit>::value) {
                o = Option(
                    o.short_name(), o.long_name(), o.argcount(),
                    (value == docopt::value()) ? docopt::value(true) : value
                );
            }
        }
        parsed.push_back(o);
    }
    return parsed;
}


std::vector<std::string> parse_section(
    const std::string& name, const std::string& source
) {
    std::string source_0;
    std::transform(
        source.begin(), source.end(), std::back_inserter(source_0),
        [](char c) { return (c == '\n') ? '\0' : c; }
    );

    std::regex pattern(
        "([^\\0]*" + name + "[^\\0]*\\0?(?:[ \\t].*?(?:\\0|$))*)",
        std::regex_constants::icase
    );

    auto matches = detail::findall(pattern, source_0);

    for(auto& match: matches) {
        std::transform(
            match.begin(), match.end(), match.begin(),
            [](char c) { return (c == '\0') ? '\n' : c; }
        );
        match = detail::strip(match);
    }

    return matches;
}


std::string formal_usage(const std::string& section) {
    auto part = detail::partition(section, ":");
    auto pu = detail::split(part.after);
    std::string value = "( ";
    for(size_t i=1; i<pu.size(); i++) {
        value += (pu[i] == pu[0]) ? ") | (" : pu[i];
        if(i+1 != pu.size()) value += " ";
    }
    value += " )";
    return value;
}


//  Parse command-line argument vector.
//
//  If options_first:
//      argv ::= [ long | shorts ]* [ argument ]* [ '--' [ argument ]* ] ;
//  else:
//      argv ::= [ long | shorts | argument ]* [ '--' [ argument ]* ] ;
//
std::vector<Pattern> parse_argv(
    Tokens tokens,
    std::vector<Option> options,
    bool options_first = false
) {
    std::vector<Pattern> parsed;
    while(!tokens.empty()) {
        if(tokens.current() == "--") {
            for(size_t i=0; i<tokens.size(); i++) {
                parsed.emplace_back(Argument("", tokens[i]));
            }
            return parsed;
        }
        else if(detail::startswith(tokens.current(), "--")) {
            auto long_options = parse_long(tokens, options);
            parsed.insert(parsed.end(), long_options.begin(), long_options.end());
        }
        else if(
            detail::startswith(tokens.current(), "-") && tokens.current() != "-"
        ) {
            auto short_options = parse_shorts(tokens, options);
            parsed.insert(parsed.end(), short_options.begin(), short_options.end());
        }
        else if(options_first) {
            for(size_t i=0; i<tokens.size(); i++) {
                parsed.emplace_back(Argument("", tokens[i]));
            }
        }
        else {
            parsed.emplace_back(Argument("", tokens.move()));
        }
    }
    return parsed;
}


std::vector<Pattern> parse_defaults(const std::string& doc) {
    std::vector<Pattern> defaults;
    for(const auto& s: parse_section("options:", doc)) {
        auto partition = detail::partition(s, ":"); // get rid of "options:"
        auto& s_partitioned = partition.after;
        std::transform(
            s_partitioned.begin(), s_partitioned.end(), s_partitioned.begin(),
            [](char c) { return (c == '\n') ? '\0' : c; }
        );

        std::regex re(R"(\0[ \t]*(-\S+?))");
        auto split = detail::split(re, s_partitioned);
        for(size_t i=1; i<split.size(); i+=2) {
            if(!split[i].empty() && split[i][0] == '-') {
                defaults.emplace_back(Option::parse(split[i] + split[i+1]));
            }
        }
    }

    return defaults;
}

} // namespace docopt


#endif // DOCOPT_DOCOPT_HPP
